This SD card image contains RISC OS and some associated software.

RISC OS is not a flavour of Linux. It's not a type of Unix. It's certainly got
nothing to do with Windows! RISC OS is its own thing - a very specialised ARM-
based operating system. So if you've not used it before, you will find it
doesn't behave quite the same way as anything else. We think it's better!

We hope you enjoy your experience with RISC OS. Find out more at:

  https://www.riscosopen.org/

-- RISC OS Open, February 2022
